package org.zerock.web.service;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class KeywordService {
    private static KeywordService ourInstance = new KeywordService();

    public static KeywordService getInstance() {
        return ourInstance;
    }

    private KeywordService() {
    }


    public List<String> find()throws Exception{

        List<String> result = new ArrayList<String>();

        String path = "https://datalab.naver.com/keyword/realtimeList.naver";
        Document doc = Jsoup.connect(path).get();

        Elements els = doc.select(".list");
        Elements titles = doc.select(".title");


        int idx = 0;
        for (Element el: els) {
            Element span = el.select("span").first();
            System.out.println(span.ownText());
            String text = span.ownText();

            if(text.trim().length() ==0){
                continue;
            }


            result.add(text);
            if (idx == 19) {
                break;
            }
        }

        return result;

    }

    public static void main(String[] args) throws Exception{
        List<String>list = KeywordService.getInstance().find();
        System.out.println(list);
    }
}
