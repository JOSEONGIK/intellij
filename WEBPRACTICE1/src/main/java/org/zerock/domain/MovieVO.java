package org.zerock.domain;

import lombok.Data;

import javax.servlet.annotation.WebServlet;
import java.util.Date;

@Data
public class MovieVO {

    private int mno;
    private String title,synop,poster;
    private Date regdate;
}
