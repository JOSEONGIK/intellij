package org.zerock.web;

import org.zerock.domain.MovieVO;
import org.zerock.dao.MovieDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/write")
public class WriteController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher =
                req.getRequestDispatcher("/WEB-INF/write.jsp");

        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        MovieVO vo = new MovieVO();
        MovieDAO dao = new MovieDAO();
        req.setCharacterEncoding("UTF-8");
        System.out.println("Register..............");
        String title = req.getParameter("title");
        String synop = req.getParameter("synop");
        String poster = req.getParameter("poster");
        System.out.println(title + ":" + synop + ":" + poster);


        vo.setTitle(title);
        vo.setSynop(synop);
        vo.setPoster(poster);

        dao.addMovie(vo);

        resp.sendRedirect("/list");
    }
}
