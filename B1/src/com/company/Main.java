package com.company;

public class Main {

    public static void main(String[] args) {

//        ItemVO[] arr = {
//                new ItemVO("100원"),
//                new ItemVO("500원"),
//                new ItemVO("1000원"),
//                new ItemVO("사오기"),
//                new ItemVO("00000원")};

        String[] arr ={"냉면","햄버거","굶기","아이스크림"};

        ItemService service = new ItemService(arr);
        service.shuffle();

        ItemUI ui = new ItemUI(service);
        ui.playGame();


    }
}