package domain;

public class Movie {

    private String title;
    private String dirctor;
    private int pop;
    private double score;

    public static void add(Movie movie) {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirctor() {
        return dirctor;
    }

    public void setDirctor(String dirctor) {
        this.dirctor = dirctor;
    }

    public int getPop() {
        return pop;
    }

    public void setPop(int pop) {
        this.pop = pop;
    }

    public double getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", dirctor='" + dirctor + '\'' +
                ", pop=" + pop +
                ", score=" + score +
                '}';
    }

    public void setScore(double score) {
        this.score = score;
    }
}
