import org.junit.Test;
import org.zerock.dao.BoardDAO;
import org.zerock.domain.BoardVO;
import org.zerock.domain.PageDTO;
import org.zerock.domain.PageMaker;

import static org.junit.Assert.assertNotNull;
import static org.zerock.domain.PageDTO.of;

public class BoardDAOTests {

    private BoardDAO boardDAO = new BoardDAO();


    @Test
    public void testRead() throws Exception{

        int bno = 5701657;

        System.out.println(boardDAO.getBoard(bno,true));

    }

    @Test
    public void testPageMaker(){

        PageDTO dto = PageDTO.of().setPage(7).setSize(10);
        int totla = 123;

        PageMaker pageMaker = new PageMaker(totla,dto);

        System.out.println(pageMaker);
    }
    @Test
    public void testList()throws Exception {

        boardDAO.getList(PageDTO.of().setPage(2).setSize(100))
                .forEach(vo -> System.out.println(vo));
    }

    @Test
    public void testInsert()throws Exception{
        BoardVO vo = new BoardVO();
        vo.setTitle("테스트 제목");
        vo.setContent("테스트 내용");
        vo.setWriter("jss");
        boardDAO.create(vo);
    }

    @Test
    public void test1(){

        assertNotNull(boardDAO);
        System.out.println("test1......................");

        PageDTO pageDTO = PageDTO.of();

        System.out.println(pageDTO);

    }
}