package org.yj;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //음식점 정보들이 존재해야 한다.
        RestaurantFinder finder = new RestaurantFinder();

        //키보드에서 먹고싶은 메뉴를 입력받는다.
        //String menu ="삼계탕";
        Scanner scanner = new Scanner(System.in);
        System.out.println("메뉴를 입력해주세요: ");
        String menu = scanner.nextLine();

        //음식점 정보들 중에서 해당 메뉴를 파는 음식점을 찾는다.
        Restaurant result = finder.findByMenu(menu);

        //출력한다.
        System.out.println(result);
    }
}
