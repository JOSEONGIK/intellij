<%--
  Created by IntelliJ IDEA.
  User: 5CLASS-184
  Date: 2018-09-12
  Time: 오후 12:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ include file="includes/header.jsp"%>
    <div id="page-wrapper" style="max-width:1000px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Movies</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Movie List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Regdate</th>
                            </tr>
                            </thead>
                            <tbody>
                           <c:forEach items="${movieList}" var="list">
                            <tr class="gradeX even">
                                <td>${list.mno}</td>
                                <td><a href="/read?mno=${list.mno}">${list.title}</a></td>
                                <td>${list.regdate}</td>
                            </tr>
                           </c:forEach>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <div class="well">
                            <a class="btn btn-primary btn-lg btn-block" target="_blank" href="/write">영화 등록하기</a>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

<%@include file="includes/footer.jsp"%>
