package org.zerock.filter;

import lombok.extern.log4j.Log4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebFilter(urlPatterns = {"/board/list"})
public class LoginCheckFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        log.info("---------------------------");
        log.info("LOGIN CHECK FILTER");
        log.info("--------------------------");

        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpServletResponse resp = (HttpServletResponse)servletResponse;

        Cookie[] cks = req.getCookies();//요청정보로 쿠키를 가져온다.

        if (cks == null || cks.length ==0){//쿠키가 저장된 배열의 길이를 가져온다.
            resp.sendRedirect("/main");
        }

        boolean check = false;

        for (Cookie ck:cks) {//쿠키의 배열을 반복으로 돌린다.
            if (ck.getName().equals("login")) {
                check = true;
                break;
            }

        }//end for

        if (!check){
            resp.sendRedirect("/main");
            return;
        }
        filterChain.doFilter(servletRequest,servletResponse);
        //이것으로 받아줘야 연결이됌
    }
}
