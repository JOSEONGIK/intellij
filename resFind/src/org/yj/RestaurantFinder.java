package org.yj;

public class RestaurantFinder {
    Restaurant[] restaurant = new Restaurant[4];

    public RestaurantFinder() {
        this.restaurant[0] = new Restaurant("이문설렁탕", new String[]{"설렁탕","곰탕","도가니탕"});
        this.restaurant[1] = new Restaurant("청진옥", new String[]{"감자탕","해장국","등뼈찜"});
        this.restaurant[2] = new Restaurant("삼양통닭", new String[]{"삼계탕","치킨","닭발"});
        this.restaurant[3] = new Restaurant("뚝배기집", new String[]{"된장찌개","김치찌개","해장국"});
    }

    public Restaurant findByMenu(String menu) {

        Restaurant result = null;

        for (int i = 0; i < restaurant.length ; i++) {
            for (int j = 0; j < this.restaurant[i].menu.length; j++) {
                if (this.restaurant[i].menu[j].contains(menu)) {
                    result = this.restaurant[i];
                }
            }
        }

        return result;
    }
}
