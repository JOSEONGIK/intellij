package Discomfort;

import java.util.Scanner;

public class DiscomfortUI {

    DiscomfortCal discomfort;
    Scanner scanner;

    public DiscomfortUI() {
        this.discomfort = new DiscomfortCal();
        scanner = new Scanner(System.in);

    }

    public void showDiscomfort(){


        System.out.println(" 온도를 입력하시오.");
        double degree = Double.parseDouble(scanner.nextLine());
        System.out.println("습도를 입력하시오.");
        double hum = Double.parseDouble(scanner.nextLine());

        double result = discomfort.calculate(degree,hum);

        if (result>=80){
            System.out.println("전원 불쾌감을 느낌.");
        }

        else if(result>=75){

            System.out.println("50%정도 불쾌감을 느낌.");
        }

        else if(result>=68){

            System.out.println("불쾌감을 나타내기 시작함.");
        }

        else{

            System.out.println("전원 쾌적함을 느낌.");
        }
    }






}
