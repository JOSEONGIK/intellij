package org.yj;

public class MatGip {

    private String place;
    private String name;
    private String menu;

    public MatGip(String place, String name, String menu) {
        this.place = place;
        this.name = name;
        this.menu = menu;
    }

    public boolean checkArea(String area){
        return this.place.contains(area);
    }

    public boolean checkName(String name){
        return this.name.contains(name);
    }

    public boolean checkMenu(String menu){
        return this.menu.contains(menu);
    }

    @Override
    public String toString() {
        return "MatGip{" +
                "area='" + place + '\'' +
                ", name='" + name + '\'' +
                ", menu='" + menu + '\'' +
                '}';
    }
}
