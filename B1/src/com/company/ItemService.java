package com.company;

import java.util.Arrays;
import java.util.Collections;

public class ItemService {
    //로직 기능은 public void 부터 시작
    //sysout, scanner 쓰면 안됨 - 그건 UI

    //여러개의 메소드에서 사용하기 때문에 인스턴스 변수 사용   * 인스턴스변수 :  계속 유지해야 하는 값
    ItemVO[] itemVOS;
    int idx;    //기본 자료형은 무조건 값을 가짐

    public ItemService(String[] arr) {
        this.itemVOS = new ItemVO[arr.length];
        for (int i = 0; i < arr.length; i++) {
            itemVOS[i] = new ItemVO(arr[i]);

        }
    }


    public ItemService(ItemVO[] itemVOS) {
        this.itemVOS = itemVOS;
        this.idx = 0; //굳이 없어도 됨
    }

    public void shuffle() { //섞기만 되기 때문에 파라미터, 리턴 타입 필요 x
        Collections.shuffle(Arrays.asList(itemVOS));   //섞는 코드    자바 리스트 사용
    }

    public ItemVO selectOne() { //랜덤하게 뽑는거기 때문에 파라미터 X 리턴 타입은 명확
        //return 은 반환한다고 생각하면 안됨!! 끝났다는 표시
        //리턴 타입은 이름없는 변수를 새로 만든다고 생각하면 됨
        ItemVO result = this.itemVOS[idx]; //기본 형태로 return 해놓으면 오류 안남

        idx++;

        return result;
    }
}
