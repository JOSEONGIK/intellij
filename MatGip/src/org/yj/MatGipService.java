package org.yj;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MatGipService {
    ArrayList<MatGip> list;

    public MatGipService() {

        list = new ArrayList<>();
    }

    public void register(MatGip obj) {

        list.add(obj);
        System.out.println(list);

    }

    public ArrayList<MatGip> findByPlace(String place) {

        ArrayList<MatGip> arr = new ArrayList<>();

        for (MatGip vo : list) {
            if (vo.checkArea(place)) {
                arr.add(vo);
            }

        }
        return arr;
    }
    public List<MatGip> findByPlace2(String place) {

        return this.list.stream().filter(x -> x.checkArea(place)).collect(Collectors.toList());

    }
}





