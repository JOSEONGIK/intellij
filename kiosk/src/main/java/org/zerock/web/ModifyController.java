package org.zerock.web;

import org.zerock.dao.MovieDAO;
import org.zerock.domain.MovieVO;
import org.zerock.dao.MovieDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/modify")
public class ModifyController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        MovieVO vo=new MovieVO();
        MovieDAO movieDAO=new MovieDAO();
        vo=movieDAO.getMovie(Integer.parseInt(req.getParameter("mno")));

        req.getServletContext().setAttribute("movie",vo);
        RequestDispatcher dispatcher=req.getRequestDispatcher("/WEB-INF/modify.jsp");


        dispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        MovieVO vo=new MovieVO();
        MovieDAO movieDAO=new MovieDAO();

        String mnoStr=req.getParameter("mno");
        String title=req.getParameter("title");
        String synop=req.getParameter("synop");
        String poster=req.getParameter("poster");

        int mno=Integer.parseInt(mnoStr);
        vo.setMno(mno);

            vo.setTitle(title);

            vo.setSynop(synop);

            vo.setPoster(poster);

        movieDAO.modifyMovie(vo);

        RequestDispatcher dispatcher=req.getRequestDispatcher("/WEB-INF/list.jsp");


        dispatcher.forward(req,resp);
    }
}
