package org.zerock.web;


import org.zerock.dao.BoardDAO;
import org.zerock.domain.BoardVO;
import org.zerock.domain.PageDTO;
import org.zerock.domain.PageMaker;
import org.zerock.util.Converter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/board/*")
public class BoardController extends AbstractController {

    private BoardDAO dao = new BoardDAO();

    public String writeGET(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("writeGET.......................");

        return "write";
    }
    public void writePOST(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("writePOST.......................");
       BoardVO vo = new BoardVO();

        req.setCharacterEncoding("UTF-8");
        System.out.println("Register..............");
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        String writer = req.getParameter("writer");



        vo.setTitle(title);
        vo.setContent(content);
        vo.setWriter(writer);

        dao.create(vo);

        resp.sendRedirect("/board/list");

    }
    public String listGET(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("listGET.......................");

        PageDTO dto = PageDTO.of()
                .setPage(Converter.getInt(req.getParameter("page"),1))
                .setSize(Converter.getInt(req.getParameter("size"),10));
        int total=320;
        PageMaker pageMaker=new PageMaker(total,dto);

        req.setAttribute("pageMaker",pageMaker);
        req.setAttribute("list", dao.getList(dto));

        return "list";
    }

    public String readGET(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("readGET.......................");
        String bnoStr=req.getParameter("bno");
        String pageStr=req.getParameter("page");


        int bno=Converter.getInt(bnoStr,-1);
        boolean updateable=false;
        if(bno==-1){
            throw  new Exception("꺼뎡(invalid data");
        }

        Cookie[] cookies=req.getCookies();
        Cookie viewCookie=null;
        for(Cookie ck:cookies){
            if(ck.getName().equals("views")){
                viewCookie=ck;
                break;
            }
        }
        //쿠키가 있다면

        //쿠키가 없다면
        if(viewCookie==null){
            Cookie newCookie=new Cookie("views",bnoStr+"%");
            newCookie.setMaxAge(60*60*24);
            viewCookie=newCookie;
            System.out.println("-----------------------------------------");
            System.out.println(viewCookie);
            updateable=true;
        }else{
            String cookieValue=viewCookie.getValue();
            updateable=cookieValue.contains(bnoStr+'%')==false;
            if(updateable==true){
                cookieValue=cookieValue+bnoStr+"%";
                viewCookie.setValue(cookieValue);
            }
        }
        resp.addCookie(viewCookie);

        req.setAttribute("board",dao.getBoard(bno,updateable));
        req.setAttribute("page",pageStr);
        return "read";
    }



    public String modifyGET(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("modifyGET.......................");
        String bnoStr=req.getParameter("bno");
        System.out.println("--------------------------------------------------------------------");
        System.out.println("bno: "+bnoStr);
        String pageStr=req.getParameter("page");
        int bno=Converter.getInt(bnoStr,-1);

        req.setAttribute("board",dao.getBoard(bno,false));
        req.setAttribute("page",pageStr);
        return "modify";
    }
    public void modifyPOST(HttpServletRequest req, HttpServletResponse resp)throws Exception {
        req.setCharacterEncoding("UTF-8");
        String pageStr=req.getParameter("page");


        String bnoStr=req.getParameter("bno");
        String title=req.getParameter("title");
        String content=req.getParameter("content");
        String writer=req.getParameter("writer");

        int bno=Integer.parseInt(bnoStr);

       BoardVO vo=new BoardVO();

        vo.setBno(bno);

        vo.setTitle(title);

        vo.setContent(content);

        vo.setWriter(writer);

        dao.modify(vo);

        resp.sendRedirect("/board/list?page="+pageStr);
    }

    public void removePOST(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        System.out.println("REMOVE POST.......................");
        String bnoStr=req.getParameter("bno");
        Integer bno=Integer.parseInt(bnoStr);
        dao.delete(bno);
        resp.sendRedirect("/board/list");
    }
    public String getBasic() {
        return "/board/";
    }
}