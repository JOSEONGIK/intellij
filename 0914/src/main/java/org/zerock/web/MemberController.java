package org.zerock.web;

import lombok.extern.log4j.Log4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;

@WebServlet("/member/*")
@Log4j
public class MemberController extends AbstractController{


    public String loginGET(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        log.info("login post");

        Cookie loginCookie = new Cookie("login", URLEncoder.encode("Jo Seong ik","UTF-8"));
        loginCookie.setMaxAge(60*60*24);//쿠키의 유효기간을 24시간으로 설정한다
        loginCookie.setPath("/");//쿠키의 유효한 다이렉트를 /로 설정한다
        resp.addCookie(loginCookie);//클라이언트 응답에 쿠키가 반응한다

        return "/board/list";
    }
    @Override
    public String getBasic() {
        return "/member/";
    }
}
