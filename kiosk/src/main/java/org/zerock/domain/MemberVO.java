package org.zerock.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class MemberVO {
    private String id,pw,name,addr;
}
