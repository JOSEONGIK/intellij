package org.zerock.web;

import org.zerock.dao.MovieDAO;
import org.zerock.domain.MovieVO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/read")
public class ReadController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        MovieVO vo=new MovieVO();
        MovieDAO movieDAO=new MovieDAO();
        vo=movieDAO.getMovie(Integer.parseInt(req.getParameter("mno")));

        req.getServletContext().setAttribute("movie",vo);

        req.getRequestDispatcher("/WEB-INF/read.jsp").forward(req,resp);
    }
}
