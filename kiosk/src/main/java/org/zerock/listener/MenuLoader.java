package org.zerock.listener;

import org.zerock.domain.MenuVO;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebListener
public class MenuLoader implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        List<MenuVO> menuVOList = new ArrayList<>();
        Map<Integer, MenuVO> menuVOMap = new HashMap<>();
        //그럼 OrderVO에 변수를 만들고 MenuLoader에서 썸을 만드는 로직을 만들어서
        //오더리저트에서 설정한 변수를 불러오는 방법?!

        for (int i = 0; i < 10; i++){
            MenuVO vo = new MenuVO(i, "메뉴"+i, i*10000);
            menuVOList.add(vo);
            menuVOMap.put(i, vo);
        }
        sce.getServletContext().setAttribute("menuList",menuVOList);
        sce.getServletContext().setAttribute("menuMap",menuVOMap);
    }
}
