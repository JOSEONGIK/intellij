package org.yj;

public class DailyWorkerMaker extends AbstractMaker{

    @Override
    public Salary make() {
        System.out.println("일용직 만들기");
        DailyWorker obj = new DailyWorker();

        obj.workHour = (int) inputI("일용직의 시간을 작성해주세요.");
        obj.pay = inputD("일용직의 시급을 작성해주세요.");


        return (obj);
    }
}
