import jr.Jrlet;

import java.io.FileInputStream;
import java.util.Properties;

public class JrletFactory {


    private Properties prop;

    public JrletFactory()throws Exception{
        String path ="C:\\webroot\\jrlet.properties";
        prop = new Properties();
        prop.load(new FileInputStream(path));

        System.out.println(prop);
    }

    public Jrlet get(String line)throws Exception{

        //  /hello /time  /bmi?we....

        int idx  = line.indexOf("?");

        String targetURL = idx == -1? line: line.substring(0, idx);

        System.out.println("-------------------------2" + targetURL);

        String className = prop.getProperty(targetURL);


        System.out.println("className: " + className);

        Object obj = Class.forName(className).newInstance();

        return (Jrlet)obj;
    }

}