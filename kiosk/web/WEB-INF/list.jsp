<%--
  Created by IntelliJ IDEA.
  User: 5CLASS-184
  Date: 2018-09-12
  Time: 오후 12:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ include file="includes/header.jsp"%>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Movies</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Movie List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Regdate</th>
                                <th>Synop</th>>
                            </tr>
                            </thead>
                            <tbody>
                           <c:forEach items="${movieList}" var="list">
                            <tr class="gradeX">
                                <td>${list.mno}</td>
                                <td><a href="/read?mno=${list.mno}"> ${list.title}</a></td>
                                <td>${list.regdate}</td>
                                <td class="center">-</td>
                                <td class="center">X</td>
                            </tr>
                           </c:forEach>
                            </tbody>
                        </table>
                        <div class="pagenation">

                            <%--시작페이지--%>
                            <fmt:parseNumber var="startPage" value="${page - (page%10) + 1}"/>
                            <c:if test="${page%10 eq 0}">
                                <fmt:parseNumber var="startPage" value="${page - 9}"/>
                            </c:if>
                            <%--끝/ 시작페이지--%>

                            <%--끝페이지--%>
                            <fmt:parseNumber var="endPage" value="${page - (page%10) + 10}"/>
                            <c:if test="${page%10 eq 0}">
                                <fmt:parseNumber var="endPage" value="${page}"/>
                            </c:if>

                            <%--끝페이지 처리--%>
                            <c:if test="${endPage gt totalPage}">
                                <fmt:parseNumber var="endPage" value="${totalPage}"/>
                            </c:if>
                            <%--끝/ 끝페이지 처리--%>
                            <%--끝/ 끝페이지--%>

                            <%--총 페이지수가 10이 안될때--%>
                            <c:if test="${totalPage le 10}">
                                <c:forEach var="i" begin="1" end="${totalPage}" step="1">
                                    <a href="?page=${i}">${i}</a>
                                </c:forEach>
                            </c:if>
                            <%--끝/ 총 페이지수가 10이 안될때--%>

                            <%--총 페이지수가 10이 넘을때--%>
                            <c:if test="${totalPage gt 10}">

                                <a href="?page=${startPage-10}">이전◀</a>

                                <c:forEach var="i" begin="${startPage}" end="${endPage}" step="1">

                                    <c:if test="${page eq i}">
                                        <strong>
                                    </c:if>

                                    <a href="?page=${i}"><span style="padding:0 3px;">${i}</span></a>

                                    <c:if test="${page eq i}">
                                        </strong>
                                    </c:if>

                                </c:forEach>
                                <a href="?page=${startPage + 10}">▶다음</a>
                            </c:if>
                            <%--끝/ 총 페이지수가 10이 넘을때--%>

                        </div>

                        <!-- /.table-responsive -->
                        <div class="well">
                            <h4>Movie Review Information</h4>
                            <p><a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>
                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="/write">Register for Review</a>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

<%@include file="includes/footer.jsp"%>
