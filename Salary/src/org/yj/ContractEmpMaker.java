package org.yj;

public class ContractEmpMaker extends AbstractMaker{

    @Override
    public Salary make() {
        System.out.println("프리랜서 만들기");
        ContractEmp obj = new ContractEmp();

        obj.yearSal = inputD("계약직의 연봉은 얼마인가요?");
        obj.lunch = inputD("계약직의 식대는 얼마인가요?");


        return (obj);
    }
}
