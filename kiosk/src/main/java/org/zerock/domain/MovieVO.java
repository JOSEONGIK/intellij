package org.zerock.domain;

import lombok.Data;

import java.util.Date;

@Data
public class MovieVO {
    private int mno,count;
    private String title,synop,poster;
    private Date regdate;

}
