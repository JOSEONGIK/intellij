package org.zerock.web;

import org.zerock.dao.MovieDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/remove")
public class RemoveController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String mno_str = req.getParameter("mno");

        MovieDAO dao = new MovieDAO();
        dao.removeMovie(Integer.parseInt(mno_str));

        resp.sendRedirect("/list");
    }
}