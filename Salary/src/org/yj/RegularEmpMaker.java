package org.yj;

public class RegularEmpMaker extends AbstractMaker {


    @Override
    public Salary make() {
        System.out.println("정규직 만들기");
        return new RegularEmp();
    }
}
