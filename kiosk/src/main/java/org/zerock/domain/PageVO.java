package org.zerock.domain;

import lombok.Data;

import java.util.Date;

@Data
public class PageVO {

    private int mno,Count;
    private String title, synop;
    private Date regdate;

   }
