package org.zerock.dao;

import org.zerock.domain.MovieVO;

import java.util.ArrayList;
import java.util.List;

public class MovieDAO {

    public MovieVO getMovie(final int mno){
        final MovieVO vo = new MovieVO();
        final String sql = "select * from tbl_movie where mno=?";

        new QueryExecutor(){

            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);
                stmt.setInt(1,mno);
                rs = stmt.executeQuery();
//con으로 연결을 시작하여 DB에 값을 넣을 준비를 함.
// 이후에 최종적인 RS를 기반으로 DB에 넣을준비를 완료하고 WHILE문에서 한행씩 값을 끄집어내서 넣어줌
                while (rs.next()){
                    vo.setMno(rs.getInt("mno"));
                    vo.setTitle(rs.getString("title"));
                    vo.setSynop(rs.getString("synop"));
                    vo.setPoster(rs.getString("poster"));
                    vo.setRegdate(rs.getDate("regdate"));
                }
            }
        }.executeAll();

        return vo;
    }

    public void removeMovie(final int mno){
        final String sql = "delete from tbl_movie\n" +
                "where mno = ?";
        new QueryExecutor(){
            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);
                stmt.setInt(1,mno);
                stmt.executeUpdate();
            }
        }.executeAll();
    }


    public List<MovieVO> getAllMovie(){

        final List<MovieVO> list = new ArrayList<>();
        final String sql = "select*from tbl_movie";

        new QueryExecutor(){

            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()){
                    MovieVO vo = new MovieVO();

                    vo.setMno(rs.getInt("mno"));
                    vo.setTitle(rs.getString("title"));
                    vo.setSynop(rs.getString("synop"));
                    vo.setPoster(rs.getString("poster"));
                    vo.setRegdate(rs.getDate("regdate"));

                    list.add(vo);
                }
            }
        }.executeAll();

        return list;
    }


}
