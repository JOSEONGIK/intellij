package domain;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GsonTest {

    //bad code
    public static void main(String[] args) throws Exception{


        Movie movie = new Movie();
        movie.setTitle("타이타닉");
        movie.setDirctor("제임스 카메론");
        movie.setPop(429511);
        movie.setScore(9.86);

        Movie movie1 = new Movie();
        movie1.setTitle("공작");
        movie1.setDirctor("윤종빈");
        movie1.setPop(4289216);
        movie1.setScore(8.00);

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie);
        movies.add(movie1);


        Gson gson = new Gson();

        String str = gson.toJson(movie);

        System.out.println(str);

    }

}
