package org.zerock.web;

import org.zerock.dao.MovieDAO;
import org.zerock.domain.MovieVO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/list")
public class ListController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        MovieDAO dao = new MovieDAO();
        List<MovieVO> movieVOList=dao.getAllMovie();

        req.setAttribute("movieList",movieVOList);
        req.getRequestDispatcher("/WEB-INF/list.jsp").forward(req,resp);

    }
}
