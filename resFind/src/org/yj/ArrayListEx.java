package org.yj;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListEx {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        //create(insert)
        list.add("AAA");
        list.add("AAAB");
        list.add("AAAC");

        System.out.println(list);

        //read
        //System.out.println(list.get(2)); //3번째 배열 가져올때(단순히가져온거)
        System.out.println(list.remove(0));//0번째꺼 가져오고 뒤에꺼 땡거옴
        System.out.println(list);

        //update
        list.set(0,"ZZZZZ");
        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        list.stream().forEach(x -> System.out.println(x));

        Collections.shuffle(list);
        System.out.println(list);
    }

}
