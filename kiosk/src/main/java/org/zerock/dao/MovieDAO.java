package org.zerock.dao;

import org.zerock.dao.QueryExecutor;
import org.zerock.domain.MovieVO;
import org.zerock.domain.PageVO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MovieDAO {


    public List<MovieVO> getPagingMovie(final int page){
        final List<MovieVO> list = new ArrayList<>();
        final String sql ="select * from\n" +
                "(select /*+INDEX_DESC(tbl_movie pk_board)*/\n" +
                "rownum rn, mno, title, synop, poster, regdate \n" +
                "from tbl_movie\n" +
                "where rownum <= ? and mno > 0)\n" +
                "where rn > ?";

        new QueryExecutor(){
            @Override
            public void doJob() throws Exception {

                stmt = con.prepareStatement(sql);
                stmt.setInt(1,page * 10);
                stmt.setInt(2,(page - 1) * 10);
                rs = stmt.executeQuery();

                while(rs.next()){
                    MovieVO vo= new MovieVO();

                    vo.setMno(rs.getInt("mno"));
                    vo.setTitle(rs.getString("title"));
                    vo.setSynop(rs.getString("synop"));
                    vo.setPoster(rs.getString("poster"));
                    vo.setRegdate(rs.getDate("regdate"));

                    list.add(vo);
                }
            }
        }.executeAll();

        return list;
    }

    public int countMovie() {
        final String sql = "select count(*) count from tbl_movie";
        new QueryExecutor() {
            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);
                rs = stmt.executeQuery();
                while (rs.next()) {
                    MovieVO vo = new MovieVO();
                    vo.setCount(rs.getInt("count"));
                    count = vo.getCount();
                }
            }
        }.executeAll();

        return count;
    }

    public MovieVO getMovie(final int mno){
        final MovieVO vo=new MovieVO();
        final String sql="select * from tbl_movie where mno=?";

        new QueryExecutor() {
            @Override
            public void doJob() throws Exception {
                stmt=con.prepareStatement(sql);
                stmt.setInt(1,mno);
                rs=stmt.executeQuery();

                while (rs.next()){
                    vo.setMno(rs.getInt("mno"));
                    vo.setTitle(rs.getString("title"));
                    vo.setSynop(rs.getString("synop"));
                    vo.setPoster(rs.getString("poster"));
                    vo.setRegdate(rs.getDate("regdate"));

                }
            }
        }.executeAll();

        return vo;
    }


    public void removeMovie(final int mno){
        final String sql = "delete from tbl_movie\n" +
                "where mno = ?";
        new QueryExecutor(){
            @Override
            public void doJob() throws SQLException {
                stmt = con.prepareStatement(sql);
                stmt.setInt(1,mno);
                stmt.executeUpdate();

            }
        }.executeAll();
    }

    public List<MovieVO> getAllMovie(){

        final List<MovieVO> list = new ArrayList<>();
        final String sql ="select\n" +
                "  /*+ index_desc(tbl_movie pk_movie) */ \n" +
                "* \n" +
                "from tbl_movie where mno>0";

        new QueryExecutor(){
            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);
                rs = stmt.executeQuery();

                while(rs.next()){
                    MovieVO vo= new MovieVO();

                    vo.setMno(rs.getInt("mno"));
                    vo.setTitle(rs.getString("title"));
                    vo.setSynop(rs.getString("synop"));
                    vo.setPoster(rs.getString("poster"));
                    vo.setRegdate(rs.getDate("regdate"));

                    list.add(vo);
                }
            }
        }.executeAll();

        return list;
    }

    public void modifyMovie(final MovieVO vo) {
        final String sql = "update tbl_movie \n" +
                "set \n" +
                " title=?,\n" +
                " synop=?,\n" +
                " poster=?\n" +
                "where mno=?\n";

        new QueryExecutor() {
            @Override
            public void doJob() throws Exception {
                stmt = con.prepareStatement(sql);


                stmt.setString(1, vo.getTitle());
                stmt.setString(2, vo.getSynop());
                stmt.setString(3, vo.getPoster());
                stmt.setInt(4, vo.getMno());
                stmt.executeUpdate();
            }
        }.executeAll();

    }

    public void addMovie(final MovieVO vo) {

        final String sql = "insert into tbl_movie(mno,title,synop,poster)\n" +
                     "values (seq_movie.nextval,?,?,?)";

        new QueryExecutor() {
            @Override
            public void doJob() throws Exception {
                stmt =con.prepareStatement(sql);

                stmt.setString(1,vo.getTitle());
                stmt.setString(2,vo.getSynop());
                stmt.setString(3,vo.getPoster());
                stmt.executeUpdate();
            }

        }.executeAll();

    }

}
