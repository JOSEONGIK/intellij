package jr;

import java.io.OutputStream;
import java.util.Map;
import java.util.Scanner;

public class Bmilet extends AbstractJrlet {
    @Override
    public void service(String line, OutputStream out) throws Exception {

        out.write(new String("Content-Type: text/html;\r\n\r\n").getBytes());
        System.out.println("bmi service");
        out.write("<h1>bmi service</h1>".getBytes());

        Map<String, String> paramMap = parse(line);

        double temp = Double.parseDouble(paramMap.get("temp"));
        double hum = Double.parseDouble(paramMap.get("hum"));


        double Discomfort = ((5/9*temp) - (0.05*(1-hum)*(5/9*temp-26)+32));

        out.write(("<h1>" + paramMap.get("temp")+"</h1>").getBytes());
        out.write(("<h1>" + paramMap.get("hum")+"</h1>").getBytes());
        out.write(("<hr/>").getBytes());
        out.write(("<h2>" + Discomfort +"</h2>").getBytes());

    }
}