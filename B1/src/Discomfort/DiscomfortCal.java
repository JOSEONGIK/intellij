package Discomfort;

public class DiscomfortCal {

    public double calculate(double degree, double hum) {

        double num = 1.8 * degree - 0.55 * ( 1 -  hum * 0.01) * ( 1.8 * degree - 26.0) + 32.0;

        return num;
    }
}
